









$('.fest-article__close').click(function(e) {
  e.preventDefault();
  $('.fest-article').fadeOut('fast');
  $('.fest-event-article').fadeOut('fast');
  $('.fest-article').removeClass('fest-article--event');
  $('.fest-city').removeClass('fest-city--active');
  $('.fest-article').removeClass('slide-left--show');
  $('.fest-map').removeClass('fest-map--article');
  $('.fest-article__events-all').fadeOut();
  $('.fest-event').removeClass('fest-event--active');
});

if ($(window).width() > 1199 && $('.fest-article').length) {
  const psArticle = new PerfectScrollbar('.fest-article');
}

var isCloned = false;

function festivalEvents() {
    
  $('.fest-article__more').click(function(e) {
    e.preventDefault();

    if ($(window).width() > 991) {
      $(this).hide();
      $('.fest-article__event--additional').fadeIn('fast');
    }
    else {
      if (isCloned == false) {
        $('.fest-article__events-list .fest-event').each(function() {
          $(this).clone().appendTo('.fest-article__events-all-wrapper');
        });
      }
      isCloned = true;
      eventFunction();
      $('.fest-article__events-all').fadeIn('fast');
      $('.fest-article').addClass('fest-article--event');
      
    }
  });
}

festivalEvents();

$('.fest-article__events-all-close').click(function(e) {
  e.preventDefault();
  $('.fest-article__events-all').fadeOut('fast');
  $('.fest-article').removeClass('fest-article--event');
  $('.fest-event-article').fadeOut('fast');
});


$('.fest-city').click(function (e) {
  e.preventDefault();
  $('.fest-city').removeClass('fest-city--active');
  $('.fest-event-article').hide();
  $(this).toggleClass('fest-city--active');
  $('.fest-article').css('display', 'flex');
  $('.fest-map').addClass('fest-map--article');
  $('.fest-article').addClass('slide-left--show');
  setTimeout(function () {
    $('.fest-article__slider').slick({
      arrows: false,
      dots: true,
      autoplay: true,
      adaptiveHeight: true
    })
  }, 10)
});

function eventFunction() {
  $('.fest-event').click(function (e) {
    e.preventDefault();
    $('.fest-event').removeClass('fest-event--active');
    $(this).toggleClass('fest-event--active');
    $('.fest-article').addClass('fest-article--event');
    $('.fest-event-article').fadeIn('fast');
  })
  
  $('.fest-article__events-all .fest-event').click(function (e) {
    e.preventDefault();
    $('.fest-event-article').addClass('fest-event-article--all');
  })
}

eventFunction();

$('.fest-event-article__close').click(function(e) {
  e.preventDefault();
  $('.fest-event-article').fadeOut('fast');
  $('.fest-event--active').removeClass('fest-event--active');
  $('.fest-article').removeClass('fest-article--event');
  $('.fest-event-article').removeClass('fest-event-article--all');
  $('.fest-article__events-all').fadeOut('fast');
});



var locations = [
  {
    lat: 55.80043412,
    lng: 37.48201732
  },
  {
    lat: 55.67957415,
    lng: 37.70174388
  },
  {
    lat: 55.92300581,
    lng: 37.77462230
  },
  {
    lat: 68.99156512,
    lng: 33.04783724
  },
  {
    lat: 56.83920031,
    lng: 53.26569204
  },
  {
    lat: 56.80908048,
    lng: 53.20252066
  }
]


function initMap() {

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 5,
    disableDefaultUI: true,
    center: {
      lat: 55.80043412,
      lng: 37.48201732
    },
    styles: [
      {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#82c0ff"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#b6ebfc"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#00c400"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ff8040"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      }
    ]
  });

  // Add some markers to the map.
  // Note: The code uses the JavaScript Array.prototype.map() method to
  // create an array of markers based on a given "locations" array.
  // The map() method here has nothing to do with the Google Maps API.
  var markers = locations.map(function (location, i) {
    return new google.maps.Marker({
      position: location,
      icon: 'img/fest-pin.svg'
    });
  });

  // Add a marker clusterer to manage the markers.
  var markerCluster = new MarkerClusterer(map, markers, {
    imagePath: 'img/fest-pin'
  });


  $('.fest-map__zoom-plus').click(function(e) {
    e.preventDefault();
    map.setZoom(map.getZoom() + 1);
  });

  $('.fest-map__zoom-minus').click(function(e) {
    e.preventDefault();
    map.setZoom(map.getZoom() - 1);
  });
  
  infoWindow = new google.maps.InfoWindow;

  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
//      infoWindow.setContent('Location found.');
      infoWindow.open(map);
      map.setCenter(pos);
    });
  }
}


//function initMMap() {
//
//  var map = new google.maps.Map(document.getElementById('map'), {
//    zoom: 5,
//    disableDefaultUI: true,
//    minZoom: 6, 
//    maxZoom: 16,
//    center: {
//      lat: 55.80043412,
//      lng: 37.48201732
//    },
//    gestureHandling: 'greedy',
//    styles: [
//      {
//        "featureType": "landscape.man_made",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#82c0ff"
//          }
//        ]
//      },
//      {
//        "featureType": "landscape.natural",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#b6ebfc"
//          }
//        ]
//      },
//      {
//        "featureType": "poi.park",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#00c400"
//          }
//        ]
//      },
//      {
//        "featureType": "road",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#ffffff"
//          }
//        ]
//      },
//      {
//        "featureType": "road.highway",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#ff8040"
//          }
//        ]
//      },
//      {
//        "featureType": "road.highway",
//        "elementType": "geometry.stroke",
//        "stylers": [
//          {
//            "color": "#ffffff"
//          }
//        ]
//      },
//      {
//        "featureType": "water",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#ffffff"
//          }
//        ]
//      }
//    ]
//  });
//
//  // Add some markers to the map.
//  // Note: The code uses the JavaScript Array.prototype.map() method to
//  // create an array of markers based on a given "locations" array.
//  // The map() method here has nothing to do with the Google Maps API.
//  var markers = locations.map(function (location, i) {
//    return new google.maps.Marker({
//      position: location,
//      icon: '/festivals/img/fest-pin.svg',
//      id: location.id
//    });
//  });
//  
//  markers.forEach(function(marker) {
//      marker.addListener('click', function() {
//        
//        openCity(marker.id);
//        
//        $('.fest-article').removeClass('fest-article--event');
//        $('.fest-city').removeClass('fest-city--active');
//        $('*[data-id="'+marker.id+'"]').toggleClass('fest-city--active');
//        
//      });
//  });
//
//  // Add a marker clusterer to manage the markers.
//  var markerCluster = new MarkerClusterer(map, markers, {
//    imagePath: '/festivals/img/fest-pin'
//  });
//
//
//  $('.fest-map__zoom-plus').click(function(e) {
//    e.preventDefault();
//    map.setZoom(map.getZoom() + 1);
//  });
//
//  $('.fest-map__zoom-minus').click(function(e) {
//    e.preventDefault();
//    map.setZoom(map.getZoom() - 1);
//  });
//  
//  infoWindow = new google.maps.InfoWindow;
//
//  // Try HTML5 geolocation.
//  if (navigator.geolocation) {
//    navigator.geolocation.getCurrentPosition(function(position) {
//      var pos = {
//        lat: position.coords.latitude,
//        lng: position.coords.longitude
//      };
//
//      infoWindow.setPosition(pos);
////      infoWindow.setContent('Location found.');
//      infoWindow.open(map);
//      map.setCenter(pos);
//    });
//  }
//}

if ($(window).width() > 1199 && $('.fest-search__content').length) {
  const ps = new PerfectScrollbar('.fest-search__content');
}

$('.fest-search__form-map').click(function(e) {
  e.preventDefault();
  $('.fest-map').addClass('fest-map--active');
});

$('.fest-map__list-btn').click(function(e) {
  e.preventDefault();
  $('.fest-map').removeClass('fest-map--active');
});

$('.fest-search-form__input').keyup(function() {
  if ($(this).val() != '') {
    $('.fest-search-form__clear').addClass('fest-search-form__clear--active');
  }
  else {
    $('.fest-search-form__clear').removeClass('fest-search-form__clear--active');
  }
})

$('.fest-search-form__clear').click(function(e) {
  e.preventDefault();
  $('.fest-search-form__input').val('');
    $(this).removeClass('fest-search-form__clear--active');
});


$('document').ready(function() {

  if ($('.fix-top').length) {
    var lastScrollTop = 0;
    var top = $('.fix-top');
    $(window).scroll(function(event) {
      var st = $(this).scrollTop();
      if (st > lastScrollTop) {
        top.removeClass('is-show').hide(0);
      }
      else if (st == lastScrollTop) {
        //do nothing 
        //In IE this is an important condition because there seems to be some instances where the last scrollTop is equal to the new one
      }
      else {
        if(!top.hasClass('is-show')){
          top.addClass('is-show').fadeIn(300);
        }
      }
      lastScrollTop = st;
    });
  }
  
});



jQuery(document).ready(function ($) {

  var time = 1.5;
  var $slick,
    isPause,
    tick,
    percentTime,
    progressValue,
    radius,
    circumference;

  $slick = $('.gallery__slider');
  
  $slick.slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    lazyLoad: 'ondemand',
    adaptiveHeight: false,
    dots: true,
    customPaging: function (slider, i) {
      var thumb = $(slider.$slides[i]).attr('href');
      return '<svg class="progress" width="62" height="62" viewBox="0 0 120 120">' +
        '<circle class="progress__meter" cx="60" cy="60" r="54" stroke-width="6" />' +
        '<circle class="progress__value" cx="60" cy="60" r="54" stroke-width="6" />' +
        '</svg>' + 
        '<img src="'+thumb+'", alt>';
    }
  });

  progressValue = $('.progress__value');
  radius = 54;
  circumference = 2 * Math.PI * radius;

  function startProgressbar() {
    resetProgressbar();
    percentTime = 0;
    isPause = false;
    tick = setInterval(interval, 30);
  }

  function progress(value) {
    var progress = value / 100;
    var dashoffset = circumference * (1 - progress);

    progressValue.css({
      'strokeDashoffset': dashoffset
    });
  }

  function interval() {
    if (isPause === false) {
      percentTime += 1 / (time + 0.1);

      progressValue.each(function () {
        progressValue.css({
          'strokeDasharray': circumference
        });
        progress(percentTime);
      });

      if (percentTime >= 100) {
        $slick.slick('slickNext');
        startProgressbar();
      }
    }
  }

  function resetProgressbar() {
    progressValue.css({
      'strokeDasharray': circumference
    });
    progress(0);
    clearTimeout(tick);
  }

  startProgressbar();

  $('.slick-dots li').on('click', function (e) {
    startProgressbar();
  });
  $slick.on('swipe', function (e) {
    startProgressbar();
  });
  
  $('[data-fancybox="gallery"]').fancybox({
    baseClass: 'fancybox--img-gallery'
  })
  
  if ($(window).width() < 768) {
  
    $('[data-fancybox="gallery"]').fancybox({
      arrows: false,
      infobar: false,
      smallBtn: true,
      fullscreen: {
        requestOnStart: true
      },
      baseClass: 'fancybox--img-gallery'
    })
  }

});








$(window).on('load', function () {
  $('#loader').fadeOut(600);
});




$(document).ready(function () {
  $('.js-menu-toggle').fancybox({
    src: '#menu',
    touch: false,
    baseClass: 'fancybox--menu',
    close: false,
    infobar: false,
    smallBtn: false,
    buttons: false,
    closeExisting: true
  }).click(function () {
    $('.js-search-toggle').removeClass('is-active');
    var btn = $(this);
    if (btn.hasClass('is-active')) {
      $.fancybox.close();
    }
    btn.find('span').toggleClass('is-active');
    btn.toggleClass('is-active');
  })
});


var lazyLoadInstance = new LazyLoad({
    elements_selector: ".lazy"
    // ... more custom settings?
});


$(document).on('click','.nav-link',function(){
  $(this).parents('.nav-tabs').animate({
    scrollLeft: this.offsetLeft
  },300);
});


$(document).ready(function(){
  $('#fix-table').tableHeadFixer({
    left : 1,
    head : false
  });
});

$(document).ready(function(){
  $('.js-custom-scroll-x').mCustomScrollbar({
    theme: 'light',
    axis: 'x',
    callbacks: {
      whileScrolling: function(){
        var left = this.mcs.left*(-1);
        $(this).find('.plan-table__fix-left').css('left',left+'px');
      }
    }
    
  });
});

$(document).ready(function(){
  $.fancybox.defaults.autoFocus=false;
});

jQuery(document).ready(function ($) {
  
  if ($('#progress-bar').length) {
    window.onscroll = function() {
      scrollFunction()
    };

    function scrollFunction() {
      var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
      var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
      var scrolled = (winScroll / height) * 100;
      document.getElementById("progress-bar").style.width = scrolled + "%";
    }
  }
  
});

if ($('.tn-atom').length) {
  if (window.DeviceOrientationEvent) {
    window.addEventListener('orientationchange', function() { location.reload(); }, false);
  }
}

$(document).ready(function(){
  $('.js-index-slider').slick({
    arrows: false,
    infinite: false,
    dots: true,
    draggable: false,
    autoplay: true,
    autoplaySpeed: 5000
  })
});

$(document).ready(function(){
  schemeScale();
});
$(window).resize(function(){
  schemeScale();
});

function schemeScale(){
  var w = $(window).outerWidth();
  var scale = 1;
  if(w<1440 && w>991){
    scale = (w-96)/13.44;
    
    $('.scheme').css({
      'transform':'scale(' + scale/100 +')',
      'marginTop': '-' + (94-scale)/2 + '%'
    });
  }else{
    $('.scheme').css({
      'transform':'scale(1)',
      'marginTop': 0
    });
  }
}


$(document).ready(function () {
  $('.js-search-toggle').fancybox({
    src: '#search',
    touch: false,
    baseClass: 'fancybox--menu',
    close: false,
    infobar: false,
    smallBtn: false,
    buttons: false,
    closeExisting: true
  }).click(function () {
    $('.js-menu-toggle').removeClass('is-active');
    var btn = $(this);
    if (btn.hasClass('is-active')) {
      $.fancybox.close();
    }
    btn.find('span').toggleClass('is-active');
    btn.toggleClass('is-active');
  });
  
  

  $('.search input.form-control').on('input', function() {
    if ($(this).val() != '') {
      $('.btn-clear').addClass('btn-clear--active');
    }

    else {
      $('.btn-clear').removeClass('btn-clear--active');
      $('#search-result').fadeOut('fast');
    }
  });

  $('.btn-clear').click(function() {
    $(this).removeClass('btn-clear--active');
    $('#search-result').fadeOut('fast');
  })
});

function demoResult() {
  $('#search-result').fadeIn('fast');
}




$(document).ready(function () {
  var confetti = $('.confetti');
  if ($(window).width() >= 1200) {
    $('.stadiums__link').on('mouseenter', function (e) {
      $(this).prepend(confetti.clone());
      $(this).find('.confetti').addClass('is-animated');
    }).on('mouseleave', function (e) {
      $(this).find('.confetti').css('opacity', 0).on('animationend webkitAnimationEnd', function () {
        $(this).remove();
      });
    });
  }
});

$('.street-view').on('click', function() {
  $(this).removeClass('street-view--explained');
});

(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('js-valid');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
          form.classList.add('was-validated');
        } else {
          // ajax и показ "спасибо"
          if (form.classList.contains('js-send-ajax')) {
            event.preventDefault();
            //-- this ajax
            const send = $(form);
            $.post(
                send.attr('action'),
                send.serialize()
            );
            showThank();
            setTimeout(function(){
              $(form).removeClass('was-validated').trigger('reset');
            })
          }
        }
      }, false);
    });
  }, false);
})();

function showThank() {
  $.fancybox.open({
    src: '#thank',
    touch: false,
    baseClass: 'fancybox--thank',
    close: false,
    infobar: false,
    smallBtn: false,
    buttons: false,
  });
  setTimeout(function () {
    $.fancybox.close();
  }, 4000)
}


$(".up").click(function (e) {
  e.preventDefault();
  $("html, body").animate({
    scrollTop: 0
  }, "slow");
  return false;
});


$(window).scroll(function () {
  var trigger = $(window).outerHeight();
  if ($(this).scrollTop() > trigger) {
    $('.up').addClass('up--active');
  }
  else {
    $('.up').removeClass('up--active');
  }
});

$('.videl-link__icon').fancybox({
  baseClass: 'fancybox--modal-video',
  smallBtn: true
})
