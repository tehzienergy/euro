(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('js-valid');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
          form.classList.add('was-validated');
        } else {
          // ajax и показ "спасибо"
          if (form.classList.contains('js-send-ajax')) {
            event.preventDefault();
            //-- this ajax
            const send = $(form);
            $.post(
                send.attr('action'),
                send.serialize()
            );
            showThank();
            setTimeout(function(){
              $(form).removeClass('was-validated').trigger('reset');
            })
          }
        }
      }, false);
    });
  }, false);
})();

function showThank() {
  $.fancybox.open({
    src: '#thank',
    touch: false,
    baseClass: 'fancybox--thank',
    close: false,
    infobar: false,
    smallBtn: false,
    buttons: false,
  });
  setTimeout(function () {
    $.fancybox.close();
  }, 4000)
}
