$(document).ready(function () {
  $('.js-menu-toggle').fancybox({
    src: '#menu',
    touch: false,
    baseClass: 'fancybox--menu',
    close: false,
    infobar: false,
    smallBtn: false,
    buttons: false,
    closeExisting: true
  }).click(function () {
    $('.js-search-toggle').removeClass('is-active');
    var btn = $(this);
    if (btn.hasClass('is-active')) {
      $.fancybox.close();
    }
    btn.find('span').toggleClass('is-active');
    btn.toggleClass('is-active');
  })
});
