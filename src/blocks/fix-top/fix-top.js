$('document').ready(function() {

  if ($('.fix-top').length) {
    var lastScrollTop = 0;
    var top = $('.fix-top');
    $(window).scroll(function(event) {
      var st = $(this).scrollTop();
      if (st > lastScrollTop) {
        top.removeClass('is-show').hide(0);
      }
      else if (st == lastScrollTop) {
        //do nothing 
        //In IE this is an important condition because there seems to be some instances where the last scrollTop is equal to the new one
      }
      else {
        if(!top.hasClass('is-show')){
          top.addClass('is-show').fadeIn(300);
        }
      }
      lastScrollTop = st;
    });
  }
  
});
