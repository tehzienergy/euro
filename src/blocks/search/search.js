$(document).ready(function () {
  $('.js-search-toggle').fancybox({
    src: '#search',
    touch: false,
    baseClass: 'fancybox--menu',
    close: false,
    infobar: false,
    smallBtn: false,
    buttons: false,
    closeExisting: true
  }).click(function () {
    $('.js-menu-toggle').removeClass('is-active');
    var btn = $(this);
    if (btn.hasClass('is-active')) {
      $.fancybox.close();
    }
    btn.find('span').toggleClass('is-active');
    btn.toggleClass('is-active');
  });
  
  

  $('.search input.form-control').on('input', function() {
    if ($(this).val() != '') {
      $('.btn-clear').addClass('btn-clear--active');
    }

    else {
      $('.btn-clear').removeClass('btn-clear--active');
      $('#search-result').fadeOut('fast');
    }
  });

  $('.btn-clear').click(function() {
    $(this).removeClass('btn-clear--active');
    $('#search-result').fadeOut('fast');
  })
});

function demoResult() {
  $('#search-result').fadeIn('fast');
}
