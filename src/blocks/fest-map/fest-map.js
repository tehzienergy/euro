var locations = [
  {
    lat: 55.80043412,
    lng: 37.48201732
  },
  {
    lat: 55.67957415,
    lng: 37.70174388
  },
  {
    lat: 55.92300581,
    lng: 37.77462230
  },
  {
    lat: 68.99156512,
    lng: 33.04783724
  },
  {
    lat: 56.83920031,
    lng: 53.26569204
  },
  {
    lat: 56.80908048,
    lng: 53.20252066
  }
]


function initMap() {

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 5,
    disableDefaultUI: true,
    center: {
      lat: 55.80043412,
      lng: 37.48201732
    },
    styles: [
      {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#82c0ff"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#b6ebfc"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#00c400"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ff8040"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffffff"
          },
          {
            "lightness": 0
          },
          {
            "saturation": -80
          },
          {
//            "gamma": 0.3
          }
        ]
      }
    ]
  });

  // Add some markers to the map.
  // Note: The code uses the JavaScript Array.prototype.map() method to
  // create an array of markers based on a given "locations" array.
  // The map() method here has nothing to do with the Google Maps API.
  var markers = locations.map(function (location, i) {
    return new google.maps.Marker({
      position: location,
      icon: 'img/fest-pin.svg'
    });
  });

  // Add a marker clusterer to manage the markers.
  var markerCluster = new MarkerClusterer(map, markers, {
    imagePath: 'img/fest-pin'
  });


  $('.fest-map__zoom-plus').click(function(e) {
    e.preventDefault();
    map.setZoom(map.getZoom() + 1);
  });

  $('.fest-map__zoom-minus').click(function(e) {
    e.preventDefault();
    map.setZoom(map.getZoom() - 1);
  });
  
  infoWindow = new google.maps.InfoWindow;

  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
//      infoWindow.setContent('Location found.');
      infoWindow.open(map);
      map.setCenter(pos);
    });
  }
}


//function initMMap() {
//
//  var map = new google.maps.Map(document.getElementById('map'), {
//    zoom: 5,
//    disableDefaultUI: true,
//    minZoom: 6, 
//    maxZoom: 16,
//    center: {
//      lat: 55.80043412,
//      lng: 37.48201732
//    },
//    gestureHandling: 'greedy',
//    styles: [
//      {
//        "featureType": "landscape.man_made",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#82c0ff"
//          }
//        ]
//      },
//      {
//        "featureType": "landscape.natural",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#b6ebfc"
//          }
//        ]
//      },
//      {
//        "featureType": "poi.park",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#00c400"
//          }
//        ]
//      },
//      {
//        "featureType": "road",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#ffffff"
//          }
//        ]
//      },
//      {
//        "featureType": "road.highway",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#ff8040"
//          }
//        ]
//      },
//      {
//        "featureType": "road.highway",
//        "elementType": "geometry.stroke",
//        "stylers": [
//          {
//            "color": "#ffffff"
//          }
//        ]
//      },
//      {
//        "featureType": "water",
//        "elementType": "geometry.fill",
//        "stylers": [
//          {
//            "color": "#ffffff"
//          }
//        ]
//      }
//    ]
//  });
//
//  // Add some markers to the map.
//  // Note: The code uses the JavaScript Array.prototype.map() method to
//  // create an array of markers based on a given "locations" array.
//  // The map() method here has nothing to do with the Google Maps API.
//  var markers = locations.map(function (location, i) {
//    return new google.maps.Marker({
//      position: location,
//      icon: '/festivals/img/fest-pin.svg',
//      id: location.id
//    });
//  });
//  
//  markers.forEach(function(marker) {
//      marker.addListener('click', function() {
//        
//        openCity(marker.id);
//        
//        $('.fest-article').removeClass('fest-article--event');
//        $('.fest-city').removeClass('fest-city--active');
//        $('*[data-id="'+marker.id+'"]').toggleClass('fest-city--active');
//        
//      });
//  });
//
//  // Add a marker clusterer to manage the markers.
//  var markerCluster = new MarkerClusterer(map, markers, {
//    imagePath: '/festivals/img/fest-pin'
//  });
//
//
//  $('.fest-map__zoom-plus').click(function(e) {
//    e.preventDefault();
//    map.setZoom(map.getZoom() + 1);
//  });
//
//  $('.fest-map__zoom-minus').click(function(e) {
//    e.preventDefault();
//    map.setZoom(map.getZoom() - 1);
//  });
//  
//  infoWindow = new google.maps.InfoWindow;
//
//  // Try HTML5 geolocation.
//  if (navigator.geolocation) {
//    navigator.geolocation.getCurrentPosition(function(position) {
//      var pos = {
//        lat: position.coords.latitude,
//        lng: position.coords.longitude
//      };
//
//      infoWindow.setPosition(pos);
////      infoWindow.setContent('Location found.');
//      infoWindow.open(map);
//      map.setCenter(pos);
//    });
//  }
//}
