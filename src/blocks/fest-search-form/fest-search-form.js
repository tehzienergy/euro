$('.fest-search-form__input').keyup(function() {
  if ($(this).val() != '') {
    $('.fest-search-form__clear').addClass('fest-search-form__clear--active');
  }
  else {
    $('.fest-search-form__clear').removeClass('fest-search-form__clear--active');
  }
})

$('.fest-search-form__clear').click(function(e) {
  e.preventDefault();
  $('.fest-search-form__input').val('');
    $(this).removeClass('fest-search-form__clear--active');
});
