$('.fest-city').click(function (e) {
  e.preventDefault();
  $('.fest-city').removeClass('fest-city--active');
  $('.fest-event-article').hide();
  $(this).toggleClass('fest-city--active');
  $('.fest-article').css('display', 'flex');
  $('.fest-map').addClass('fest-map--article');
  $('.fest-article').addClass('slide-left--show');
  setTimeout(function () {
    $('.fest-article__slider').slick({
      arrows: false,
      dots: true,
      autoplay: true,
      adaptiveHeight: true
    })
  }, 10)
});
