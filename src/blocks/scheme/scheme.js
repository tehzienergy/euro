$(document).ready(function(){
  schemeScale();
});
$(window).resize(function(){
  schemeScale();
});

function schemeScale(){
  var w = $(window).outerWidth();
  var scale = 1;
  if(w<1440 && w>991){
    scale = (w-96)/13.44;
    
    $('.scheme').css({
      'transform':'scale(' + scale/100 +')',
      'marginTop': '-' + (94-scale)/2 + '%'
    });
  }else{
    $('.scheme').css({
      'transform':'scale(1)',
      'marginTop': 0
    });
  }
}
