$('.fest-article__close').click(function(e) {
  e.preventDefault();
  $('.fest-article').fadeOut('fast');
  $('.fest-event-article').fadeOut('fast');
  $('.fest-article').removeClass('fest-article--event');
  $('.fest-city').removeClass('fest-city--active');
  $('.fest-article').removeClass('slide-left--show');
  $('.fest-map').removeClass('fest-map--article');
  $('.fest-article__events-all').fadeOut();
  $('.fest-event').removeClass('fest-event--active');
});

if ($(window).width() > 1199 && $('.fest-article').length) {
  const psArticle = new PerfectScrollbar('.fest-article');
}

var isCloned = false;

function festivalEvents() {
    
  $('.fest-article__more').click(function(e) {
    e.preventDefault();

    if ($(window).width() > 991) {
      $(this).hide();
      $('.fest-article__event--additional').fadeIn('fast');
    }
    else {
      if (isCloned == false) {
        $('.fest-article__events-list .fest-event').each(function() {
          $(this).clone().appendTo('.fest-article__events-all-wrapper');
        });
      }
      isCloned = true;
      eventFunction();
      $('.fest-article__events-all').fadeIn('fast');
      $('.fest-article').addClass('fest-article--event');
      
    }
  });
}

festivalEvents();

$('.fest-article__events-all-close').click(function(e) {
  e.preventDefault();
  $('.fest-article__events-all').fadeOut('fast');
  $('.fest-article').removeClass('fest-article--event');
  $('.fest-event-article').fadeOut('fast');
});
