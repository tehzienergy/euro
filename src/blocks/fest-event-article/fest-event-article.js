$('.fest-event-article__close').click(function(e) {
  e.preventDefault();
  $('.fest-event-article').fadeOut('fast');
  $('.fest-event--active').removeClass('fest-event--active');
  $('.fest-article').removeClass('fest-article--event');
  $('.fest-event-article').removeClass('fest-event-article--all');
  $('.fest-article__events-all').fadeOut('fast');
});
