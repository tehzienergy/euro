$(".up").click(function (e) {
  e.preventDefault();
  $("html, body").animate({
    scrollTop: 0
  }, "slow");
  return false;
});


$(window).scroll(function () {
  var trigger = $(window).outerHeight();
  if ($(this).scrollTop() > trigger) {
    $('.up').addClass('up--active');
  }
  else {
    $('.up').removeClass('up--active');
  }
});
