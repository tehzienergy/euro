jQuery(document).ready(function ($) {

  var time = 1.5;
  var $slick,
    isPause,
    tick,
    percentTime,
    progressValue,
    radius,
    circumference;

  $slick = $('.gallery__slider');
  
  $slick.slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    lazyLoad: 'ondemand',
    adaptiveHeight: false,
    dots: true,
    customPaging: function (slider, i) {
      var thumb = $(slider.$slides[i]).attr('href');
      return '<svg class="progress" width="62" height="62" viewBox="0 0 120 120">' +
        '<circle class="progress__meter" cx="60" cy="60" r="54" stroke-width="6" />' +
        '<circle class="progress__value" cx="60" cy="60" r="54" stroke-width="6" />' +
        '</svg>' + 
        '<img src="'+thumb+'", alt>';
    }
  });

  progressValue = $('.progress__value');
  radius = 54;
  circumference = 2 * Math.PI * radius;

  function startProgressbar() {
    resetProgressbar();
    percentTime = 0;
    isPause = false;
    tick = setInterval(interval, 30);
  }

  function progress(value) {
    var progress = value / 100;
    var dashoffset = circumference * (1 - progress);

    progressValue.css({
      'strokeDashoffset': dashoffset
    });
  }

  function interval() {
    if (isPause === false) {
      percentTime += 1 / (time + 0.1);

      progressValue.each(function () {
        progressValue.css({
          'strokeDasharray': circumference
        });
        progress(percentTime);
      });

      if (percentTime >= 100) {
        $slick.slick('slickNext');
        startProgressbar();
      }
    }
  }

  function resetProgressbar() {
    progressValue.css({
      'strokeDasharray': circumference
    });
    progress(0);
    clearTimeout(tick);
  }

  startProgressbar();

  $('.slick-dots li').on('click', function (e) {
    startProgressbar();
  });
  $slick.on('swipe', function (e) {
    startProgressbar();
  });
  
  $('[data-fancybox="gallery"]').fancybox({
    baseClass: 'fancybox--img-gallery'
  })
  
  if ($(window).width() < 768) {
  
    $('[data-fancybox="gallery"]').fancybox({
      arrows: false,
      infobar: false,
      smallBtn: true,
      fullscreen: {
        requestOnStart: true
      },
      baseClass: 'fancybox--img-gallery'
    })
  }

});
