if ($(window).width() > 1199 && $('.fest-search__content').length) {
  const ps = new PerfectScrollbar('.fest-search__content');
}

$('.fest-search__form-map').click(function(e) {
  e.preventDefault();
  $('.fest-map').addClass('fest-map--active');
});

$('.fest-map__list-btn').click(function(e) {
  e.preventDefault();
  $('.fest-map').removeClass('fest-map--active');
});
