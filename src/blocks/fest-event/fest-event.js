function eventFunction() {
  $('.fest-event').click(function (e) {
    e.preventDefault();
    $('.fest-event').removeClass('fest-event--active');
    $(this).toggleClass('fest-event--active');
    $('.fest-article').addClass('fest-article--event');
    $('.fest-event-article').fadeIn('fast');
  })
  
  $('.fest-article__events-all .fest-event').click(function (e) {
    e.preventDefault();
    $('.fest-event-article').addClass('fest-event-article--all');
  })
}

eventFunction();
