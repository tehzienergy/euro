$(document).ready(function(){
  $('.js-index-slider').slick({
    arrows: false,
    infinite: false,
    dots: true,
    draggable: false,
    autoplay: true,
    autoplaySpeed: 5000
  })
});
