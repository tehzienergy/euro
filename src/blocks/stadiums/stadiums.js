$(document).ready(function () {
  var confetti = $('.confetti');
  if ($(window).width() >= 1200) {
    $('.stadiums__link').on('mouseenter', function (e) {
      $(this).prepend(confetti.clone());
      $(this).find('.confetti').addClass('is-animated');
    }).on('mouseleave', function (e) {
      $(this).find('.confetti').css('opacity', 0).on('animationend webkitAnimationEnd', function () {
        $(this).remove();
      });
    });
  }
});
