$(document).ready(function(){
  $('.js-custom-scroll-x').mCustomScrollbar({
    theme: 'light',
    axis: 'x',
    callbacks: {
      whileScrolling: function(){
        var left = this.mcs.left*(-1);
        $(this).find('.plan-table__fix-left').css('left',left+'px');
      }
    }
    
  });
});
